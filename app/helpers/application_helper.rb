module ApplicationHelper

  def logged_in?
    session[:user_id].present?
  end

  def current_user
    return unless logged_in?
    @current_user ||= User.find(session[:user_id])
  end

  def date_card_opts(date)
    { month: date.strftime('%b'), day: date.day }
  end

  # Is this useful?
  # def cached_css(stylesheet)
  #   css_file = File.join(ApplicationController.settings.root, 'public', 'css', "#{stylesheet}.css")
  #   expire_time = File.mtime(css_file).to_i
  #   "<link href='/css/#{stylesheet}.css?#{expire_time}' rel='stylesheet'>"
  # end

  def flash_message_clear
    session[:flash_message] = nil
    session[:flash_message_type] = nil
  end

  def flash_success(msg)
    session[:flash_message] = msg
    session[:flash_message_type] = 'flash-message-success'
  end

  def flash_fail(msg)
    session[:flash_message] = msg
    session[:flash_message_type] = 'flash-message-fail'
  end

  def format_datetime(start_time, end_time)
    start_time.getlocal.strftime('%A, %B %d ') +
      start_time.getlocal.strftime('(%I:%M%p - ') +
      end_time.getlocal.strftime('%I:%M%p)')
  end

  def send_email(msg_subject, msg_body, contact_ids)
    emails = current_user.group.contacts.where(id: contact_ids).map(&:email)

    Mail.deliver do
      from     SMTP_OPTS[:user_name]
      bcc      emails
      subject  msg_subject
      body     msg_body
    end
  end

  def send_sms(msg, contact_ids)
    phone_nums = current_user.group.contacts.where(id: contact_ids).map(&:phone)
    # TODO: parallelize this?
    Texting.new.send_sms(phone_nums, msg)
  end

  def selectable_groups
    current_user && current_user.groups.order(:name) || []
  end
end
