class SmsList < ActiveRecord::Base

  has_many :sms_subscribers
  has_many :sms_users, through: :sms_subscribers

  CMDS = [
    { name: 'responder_list',
      roles: ['responder'],
      desc:  'Show authorized responders for active list',
      usage: 'responder list' },
    { name: 'user_list',
      roles: ['responder'],
      desc:  'Show authorized senders for active list',
      usage: 'user list' },
  ]

  def send_sms(msg)
    phone_nums = SmsSubscriber.where(sms_list_id: self.id,
                                     user_type: 'responder').
                   map { |sub| sub.sms_user.phone }
    raise "No responders configured for list" if phone_nums.empty?

    Texting.new.send_sms(phone_nums, msg)
  end

  # ----------------------------------------------------------------------
  #  Methods for sms API; these correspond to defs in sms_controller.rb
  def responder_list
    sms_user_list('responder')
  end

  def user_list
    sms_user_list('sender')
  end

  private

  def sms_user_list(type)
    user_names = SmsSubscriber.where(sms_list_id: id, user_type: type).
      map { |sub| sub.sms_user.name }.
      join(', ')

    "List #{type.pluralize} for #{name}: #{user_names}"
  end

end
