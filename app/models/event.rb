class Event < ActiveRecord::Base
  has_many :events_groups
  has_many :groups, through: :events_groups

  def url
    "/events/#{event_url}"
  end
end
