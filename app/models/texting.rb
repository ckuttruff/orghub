# coding: utf-8
require 'plivo'
include Plivo

class Texting
  COUNTRY_CODE = '1'
  PHONE_DIGITS = 11

  CMDS  = [ { name: 'help',
              roles: [:all],
              desc:  "Show available commands / usage",
              usage: "help | help user | help user create" } ]

  def initialize
    @client = RestClient.new(ENV['TXT_ID'], ENV['TXT_AUTH']);
    @txt_num = ENV['TXT_NUM']
  end

  def send_sms(numbers, msg)
    formatted_nums = numbers.map{ |n| Texting.format_number(n) }
    @client.messages.create(@txt_num, formatted_nums, msg)
  end

  # plivo is picky about format of destination number ¯\_(ツ)_/¯
  def self.format_number(number)
    # strip out any non-numeric characters
    num = number.gsub(/[^0-9]+/, '')
    # prepend country code
    num.prepend(COUNTRY_CODE) if num.length < PHONE_DIGITS
    num
  end

  def self.has_access?(user, method)
    return true if user.admin?

    commands = Texting.sms_models.map { |m| m::CMDS }.flatten
    cmd_h    = commands.detect { |cmd| cmd[:name] == method.to_s }

    raise "Method not found: #{method.to_s}" unless cmd_h

    cmd_h[:roles].include?('all') || cmd_h[:roles].include?(user.user_type)
  end

  def self.help(cmd = nil, sub_cmd = nil)
    response = ""

    Texting.sms_models.each do |model|
      model::CMDS.each do |h|
        if sub_cmd
          # display command name, description, usage
          cmd_matches = (h[:name] == "#{cmd}_#{sub_cmd}")
          return "#{h[:desc]}\n#{h[:usage]}" if cmd_matches
        elsif cmd
          # help list - display usage for related commands
          response << "#{h[:usage]}\n" if h[:name] =~ /^#{cmd}/
        else
          # help - display toplevel help
          response << "#{h[:usage]}\n"
        end
      end
    end
    response
  end

  def self.sms_models
    [SmsUser, SmsList, Texting]
  end

end
