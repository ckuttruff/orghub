class SmsUser < ActiveRecord::Base

  # sms_list_id is the current active list
  belongs_to :sms_list

  has_many :sms_subscribers
  has_many :sms_lists, through: :sms_subscribers

  ROLES = [:all, :responder, :sender]
  CMDS  = [
    { name: 'list_active',
      roles: ['all'],
      desc:  'Display or set active list',
      usage: 'list active <list name>' },
    { name: 'list_add',
      roles: ['responder'],
      desc:  'Add user to active list',
      usage: 'list add <username>' },
    { name: 'list_create',
      roles: ['responder'],
      desc:  'Create new list',
      usage: 'list create <list name>' },
    { name: 'responder_add',
      roles: ['responder'],
      desc:  'Add responder to active list',
      usage: 'responder add <username>' },
    { name: 'user_create',
      roles: ['responder'],
      desc:  'Create new user for list',
      usage: 'user create <username> <phone number>' },
  ]

  def responder?(list = self.sms_list)
    u = SmsSubscriber.where(user_type: 'responder',
                            sms_user_id: id,
                            sms_list_id: list.id).first
    !u.nil?
  end

  # ----------------------------------------------------------------------
  #  Methods for sms API; these correspond to defs in sms_controller.rb

  def list_active(list_name = nil)
    # Display current name of list if we're not setting it
    msg = "Current active list: #{sms_list.name}"
    return msg unless list_name.present?

    l = SmsList.where(name: list_name).first
    # return not found message if we couldn't find list by name
    return "List name not found: #{list_name}" unless l

    # otherwise all good, update active list, display message
    self.update_attribute(:sms_list_id, l.id)
    "Active list updated to: #{list_name}"
  end

  # type is currently sender or responder
  def list_add(name)
    auth_msg = "Not authorized to add users to current list"
    return auth_msg unless admin? || responder?

    user = SmsUser.where(name: name).first
    return "User not found: #{name}" unless user

    opts = { sms_list_id: sms_list_id,
             sms_user_id: user.id,
             user_type: 'sender' }
    SmsSubscriber.create(opts)
    "Added user to list: #{name}"
  end

  def list_create(list_name)
    auth_msg = 'Not authorized to create new lists'
    return auth_msg unless admin?

    l = SmsList.create(name: list_name)
    "Created list: #{l.name}"
  end

  def responder_add(user_name)
    auth_msg = "Not authorized to create responders for current list"
    return auth_msg unless admin? || responder?

    u = SmsUser.where(name: user_name).first
    # return not found message if we couldn't find list by name
    return "User not found: #{user_name}" unless u

    opts = { sms_list_id: sms_list_id,
             sms_user_id: u.id,
             user_type: 'responder' }
    SmsSubscriber.create(opts)
    "Added responder to active list: #{user_name}"
  end

  def user_create(user_name, phone_num)
    auth_msg = "Not authorized to create users for current list"
    return auth_msg unless admin? || responder?

    SmsUser.create(name: user_name, phone: phone_num, sms_list_id: sms_list_id)
    "Created user: #{user_name}"
  end

  def user_list(user_name, phone_num)
    auth_msg = "Not authorized to create users for current list"
    return auth_msg unless admin? || responder?

    SmsUser.create(name: user_name, phone: phone_num, sms_list_id: sms_list_id)
    "Created user: #{user_name}"
  end

end
