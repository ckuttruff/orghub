class User < ActiveRecord::Base
  has_secure_password

  has_many :groups_users
  has_many :groups, through: :groups_users

  has_many :calendar_events_users
  has_many :calendar_events, through: :calendar_events_users

  def group
    self.groups.first
  end

  def group_id
    group && group.id
  end

  def contacts
    # hack to make sure empty result still quacks like ORM
    group && group.contacts || Contact.where(id: nil)
  end
end
