class CalendarEvent < ActiveRecord::Base
  has_many :calendar_events_groups
  has_many :calendar_events_users

  has_many :groups, through: :calendar_events_groups
  has_many :users,  through: :calendar_events_users
end
