require 'uri'

class CalendarController < ApplicationController

  get '/calendar' do
    @page_title = 'Calendar'

    @events = current_user.calendar_events.map do |event|
      { 'title'       => event.title,
        'description' => event.description,
        'location'    => map_link(event.location),
        'timeStr'     => format_datetime(event.start_time.localtime,
                                         event.end_time.localtime),
        'start'       => event.start_time,
        'end'         => event.end_time,
        'eventUrl'    => "<a href='#{event.event_url}'>#{request.host}#{event.event_url}</a>" }
    end.to_json

    haml :'calendar/index'
  end

  post '/calendar/create' do
    event = CalendarEvent.create(params)
    current_user.calendar_events << event

    redirect '/calendar'
  end

  # Add event from detail page to calendar
  post '/calendar/add_event' do
    event = Event.find(params[:event_id])

    ce = CalendarEvent.create(
      location:    event.location,
      start_time:  event.start_time.localtime,
      end_time:    event.end_time.localtime,
      title:       event.title,
      event_url:   event.url
    )
    current_user.calendar_events << ce

    flash_success("Created event: " + event.title)
    redirect '/calendar'
  end

  private

  def map_link(address)
    addr = URI::encode(address.gsub(' ', '+'))
    url = "https://duckduckgo.com/?q=#{addr}&t=hj&ia=maps&iaxm=maps"
    "<a href='#{url}'>#{address}</a>"
  end
end
