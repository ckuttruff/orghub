class GroupsController < ApplicationController

  get '/orgs' do
    @page_title = 'Organizations'
    # TODO: scope this to @current_user
    @groups = Group.order(:name)
    haml :'groups/index'
  end

  post '/groups/set' do
    g = Group.find(params[:target_group])
    flash_success('Active group now: ' + g.name)
    session[:current_group_id] = g.id
    redirect '/'
  end

  post '/groups/create' do
    #Group.create(h)
  end

end
