require 'encrypted_cookie'
require 'haml'
require 'json'
require 'rack/protection'
require 'securerandom'
require 'sinatra/activerecord'
require 'sinatra/base'

# application_controller.rb
class ApplicationController < Sinatra::Base
  SRC_ROOT = File.expand_path('../../..', __FILE__)
  PUBLIC_ROUTES = ['/', '/login', '/sms/receive']
  # this is a regex matcher as opposed to an exact route url
  PUBLIC_PATHS  = [/^\/events/]

  configure do
    # set folder for templates to ../views, but make the path absolute
    set :views, File.join(SRC_ROOT, 'app', 'views')
    set :root, SRC_ROOT
    set :server, :puma
    # set :haml, { escape_html: true }

    use Rack::Session::EncryptedCookie,
        secret: ENV.fetch('SESSION_SECRET') { SecureRandom.hex(64) },
        expire_after: (60 * 60 * 24 * 7) # 7 days

    use Rack::Protection
    use Rack::Protection::StrictTransport
    use Rack::Protection::AuthenticityToken

    enable :logging

    # To debug production issues; shamelessly copy-pasted
    #   TODO: i dunno, figure out how to log properly with sinatra
    if(ENV['LOGGING'] == 'true')
      Dir.mkdir('logs') unless File.exist?('logs')

      $logger = Logger.new('logs/common.log','weekly')
      $logger.level = Logger::WARN

      # Spit stdout and stderr to a file during production
      # in case something goes wrong
      $stdout.reopen("logs/output.log", "w")
      $stdout.sync = true
      $stderr.reopen($stdout)
    end
  end

  # Require config files and code under /app
  require File.join(SRC_ROOT, 'config/smtp.rb')
  Dir.glob('app/{helpers,controllers,models}/*.rb').each do |relative_path|
    file = File.join(SRC_ROOT, relative_path)
    require file
  end
  helpers ApplicationHelper

  before do
    content_type 'text/html'

    @page_title = nil
    @nav_links = [
      { path: '/orgs',     name: 'Organizations' },
      { path: '/calendar', name: 'Calendar' },
      { path: '/contacts', name: 'Contacts' },
      { path: '/events',   name: 'Events' }
    ]

    can_access = (logged_in? ||
                  PUBLIC_ROUTES.include?(request.path_info) ||
                  PUBLIC_PATHS.any? { |path| request.path_info =~ path })

    unless(can_access)
      session[:return_path] = request.path_info
      flash_fail('Please login to proceed.')
      redirect '/login'
    end
  end

  get '/' do
    redirect '/events'
  end

end
