require 'uri'

class SmsController < ApplicationController

  get '/sms/receive' do
    @from = Texting.format_number(params['From'])
    @msg  = params['Text']

    @user = SmsUser.where(phone: @from).first
    return ("Number not authorized: #{@from} \n" +
            "Please contact an administrator") unless @user

    if(@msg =~ /^\s*help/i)
      @resp = help(get_args(@msg, 1))
    else
      cmd = get_command(@msg)

      # Send text to list responders if not an explicit command
      return @user.sms_list.send_sms(@msg) unless cmd

      auth_msg = "Not authorized to perform this action; please contact an administrator"

      @resp = Texting.has_access?(@user, cmd) ?
                self.public_send(cmd, get_args(@msg)) : auth_msg
      # TODO
      # Batch out response texts so it doesn't truncate awkwardly for help output
    end

    Texting.new.send_sms([@from], @resp)
  end

  # --------------------------------------------------------------
  # These methods make up the sms texting interface for users;
  #   they correspond to definitions in their respective models
  def help(args)
    cmd, sub_cmd = args
    Texting.help(cmd, sub_cmd)
  end

  def list_active(args)
    list_name = args.join(' ')
    @user.list_active(list_name)
  end

  def list_add(args)
    user_name = args.first
    @user.list_add(user_name)
  end

  def list_create(args)
    list_name = args.join(' ')
    @user.list_create(list_name)
  end

  def responder_add(args)
    username = args.first
    @user.responder_add(username)
  end

  def responder_list(_)
    @user.sms_list.responder_list
  end

  def user_create(args)
    name, number = args
    @user.user_create(name, number)
  end

  def user_list(_)
    @user.sms_list.user_list
  end

  private

  def get_args(msg, idx = 2)
    msg.split[idx..-1]
  end

  def get_command(msg)
    cmd, sub_cmd = msg.split.map(&:downcase)
    cmd_str = sub_cmd ? "#{cmd}_#{sub_cmd}" : cmd

    valid_command?(cmd_str) && cmd_str.to_sym
  end

  def valid_command?(cmd_str)
    Texting.sms_models.each do |model|
      model::CMDS.each do |cmd_hash|
        return true if cmd_hash[:name] == cmd_str
      end
    end

    return false
  end

end
