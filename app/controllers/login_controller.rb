class LoginController < ApplicationController

  get '/login' do
    haml :login
  end

  post '/login' do
    user = User.find_by(email: params['email'])
    is_authd = (user && user.authenticate(params['password']))

    if is_authd
      flash_success('login success')
      session[:user_id] = user.id
      session[:current_group_id] = user.groups.order(:name).first.id

      return_path = session[:return_path] || '/'
      session[:return_path] = nil

      redirect return_path
    else
      flash_fail('login failed')
      redirect '/login'
    end
  end

  get '/logout' do
    @current_user = session[:user_id] = nil
    redirect '/'
  end

end
