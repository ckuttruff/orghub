# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_08_004301) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "calendar_events", force: :cascade do |t|
    t.string "title", null: false
    t.text "description"
    t.string "location"
    t.datetime "start_time", null: false
    t.datetime "end_time", null: false
    t.datetime "created_at", null: false
    t.string "recurring"
    t.string "event_url"
    t.index ["title"], name: "index_calendar_events_on_title"
  end

  create_table "calendar_events_groups", force: :cascade do |t|
    t.bigint "calendar_event_id", null: false
    t.bigint "group_id", null: false
    t.index ["calendar_event_id", "group_id"], name: "index_calendar_events_groups_on_calendar_event_id_and_group_id", unique: true
    t.index ["group_id"], name: "index_calendar_events_groups_on_group_id"
  end

  create_table "calendar_events_users", force: :cascade do |t|
    t.bigint "calendar_event_id", null: false
    t.bigint "user_id", null: false
    t.index ["calendar_event_id", "user_id"], name: "index_calendar_events_users_on_calendar_event_id_and_user_id", unique: true
    t.index ["user_id"], name: "index_calendar_events_users_on_user_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "name", null: false
    t.string "email", null: false
    t.string "phone"
    t.string "zip"
    t.text "notes"
    t.datetime "created_at", null: false
    t.index ["email"], name: "index_contacts_on_email", unique: true
  end

  create_table "contacts_groups", id: false, force: :cascade do |t|
    t.bigint "group_id", null: false
    t.bigint "contact_id", null: false
    t.datetime "created_at", null: false
    t.index ["contact_id"], name: "index_contacts_groups_on_contact_id"
    t.index ["group_id", "contact_id"], name: "index_contacts_groups_on_group_id_and_contact_id", unique: true
    t.index ["group_id"], name: "index_contacts_groups_on_group_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "banner_url"
    t.string "event_url", null: false
    t.string "title", null: false
    t.text "description", null: false
    t.string "location"
    t.datetime "start_time", null: false
    t.datetime "end_time", null: false
    t.datetime "created_at", null: false
    t.index ["event_url"], name: "index_events_on_event_url"
    t.index ["title"], name: "index_events_on_title"
  end

  create_table "events_groups", force: :cascade do |t|
    t.bigint "event_id", null: false
    t.bigint "group_id", null: false
    t.index ["event_id", "group_id"], name: "index_events_groups_on_event_id_and_group_id", unique: true
    t.index ["group_id"], name: "index_events_groups_on_group_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.index ["name"], name: "index_groups_on_name", unique: true
  end

  create_table "groups_users", id: false, force: :cascade do |t|
    t.bigint "group_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.index ["group_id", "user_id"], name: "index_groups_users_on_group_id_and_user_id", unique: true
    t.index ["group_id"], name: "index_groups_users_on_group_id"
    t.index ["user_id"], name: "index_groups_users_on_user_id"
  end

  create_table "sms_lists", force: :cascade do |t|
    t.string "name"
    t.index ["name"], name: "index_sms_lists_on_name", unique: true
  end

  create_table "sms_subscribers", force: :cascade do |t|
    t.string "user_type", default: "sender", null: false
    t.bigint "sms_list_id", null: false
    t.bigint "sms_user_id", null: false
    t.index ["sms_list_id"], name: "index_sms_subscribers_on_sms_list_id"
    t.index ["sms_user_id"], name: "index_sms_subscribers_on_sms_user_id"
    t.index ["user_type", "sms_list_id", "sms_user_id"], name: "unique_sms_subsciber_idx", unique: true
  end

  create_table "sms_users", force: :cascade do |t|
    t.string "name", null: false
    t.string "phone", null: false
    t.boolean "admin", default: false, null: false
    t.bigint "sms_list_id", null: false
    t.index ["name"], name: "index_sms_users_on_name", unique: true
    t.index ["phone"], name: "index_sms_users_on_phone", unique: true
    t.index ["sms_list_id"], name: "index_sms_users_on_sms_list_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "name", null: false
    t.string "password_digest"
    t.boolean "admin", default: false, null: false
    t.datetime "created_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "calendar_events_groups", "calendar_events"
  add_foreign_key "calendar_events_groups", "groups"
  add_foreign_key "calendar_events_users", "calendar_events"
  add_foreign_key "calendar_events_users", "users"
  add_foreign_key "contacts_groups", "contacts"
  add_foreign_key "contacts_groups", "groups"
  add_foreign_key "events_groups", "events"
  add_foreign_key "events_groups", "groups"
  add_foreign_key "groups_users", "groups"
  add_foreign_key "groups_users", "users"
  add_foreign_key "sms_subscribers", "sms_lists"
  add_foreign_key "sms_subscribers", "sms_users"
end
