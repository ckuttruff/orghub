class AddCalendarEvents < ActiveRecord::Migration[5.1]
  def up
    create_table :calendar_events do |t|
      t.string :title, null: false, index: true
      t.text :description
      t.string :location

      t.datetime :start_time, null: false
      t.datetime :end_time, null: false
      t.datetime :created_at, null: false
    end

    # allow for calendar events at an individual and org level
    %w(group user).each do |type|
      create_table :"calendar_events_#{type}s" do |t|
        t.references :calendar_event, index: false, null: false
        t.references :"#{type}", index: true, null: false
      end
      add_index :"calendar_events_#{type}s", [:calendar_event_id, :"#{type}_id"], unique: true
      add_foreign_key :"calendar_events_#{type}s", :calendar_events
      add_foreign_key :"calendar_events_#{type}s", :"#{type}s"
    end
  end

  def down
    drop_table :calendar_events_groups
    drop_table :calendar_events_users
    drop_table :calendar_events
  end
end
