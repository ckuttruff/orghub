class AddUrlToCalendarEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :calendar_events, :event_url, :string
  end
end
