class RefactorSms < ActiveRecord::Migration[5.1]
  def change
    create_table :sms_lists do |t|
      t.string :name, index: { unique: true }
    end

    create_table :sms_users do |t|
      t.string :name,  index: { unique: true }, null: false
      t.string :phone, index: { unique: true }, null: false
      t.boolean :admin, default: false, null: false
      t.references :sms_list, index: true, null: false
    end

    create_table :sms_subscribers do |t|
      t.string :user_type, null: false, default: 'sender'
      t.references :sms_list, index: true, null: false, foreign_key: true
      t.references :sms_user, index: true, null: false, foreign_key: true
    end
    add_index :sms_subscribers, [:user_type, :sms_list_id, :sms_user_id],
              unique: true, name: 'unique_sms_subsciber_idx'
  end
end
