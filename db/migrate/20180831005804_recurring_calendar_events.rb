class RecurringCalendarEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :calendar_events, :recurring, :string
  end
end
