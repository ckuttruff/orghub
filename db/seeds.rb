require_relative '../app/controllers/application_controller.rb'


def clear_data
  # wipe out existing test data
  models = [User, Contact, ContactsGroup, Group, GroupsUser, Event]
  models.each { |m| m.delete_all }
end


def rando_time
  yrs, mos, days, hrs = [[2018], 1.upto(12), 1.upto(28), 1.upto(24)].map(&:to_a)
  Time.new(yrs.sample, mos.sample, days.sample, hrs.sample)
end

def create_groups
  groups = ['PDX Outreach for Justice', 'Not a Real Group', 'Kindergartners Against Cops', 'Pizza Not Capitalism',
            'Can We Please Send Elon Musk to Mars, tho?', 'Another Really Cool Thing', 'Really just want about 12 Groups',
            'To Test Pagination and Shit', 'Tahts Not 2 Boring to Look At', 'Nifty Anarcho Doggo Crew', 'Cats Against Coppers']
  groups.each { |g| Group.create(name: g, description: g) }
end

def create_contacts
  names = ['Chris', 'Shawn', 'Alex', 'McKensey', 'Sinead', 'Comrade', 'Javier',
           'Candice', 'Kwame', 'Vanessa', 'Ella', 'Jake']
  names.each do |n|
    nl = n.downcase
    email = "#{nl}@example.org"

    opts = { email: email, name: n, password: nl, password_confirmation: nl }
    opts[:admin] = true if n == 'Chris'

    User.create(opts)

    s = ''
    10.times { s << (0..9).to_a.sample.to_s }
    Contact.create(email: email, name: n, phone: "#{s[0..2]}-#{s[3..5]}-#{s[6..-1]}", zip: '97212')
  end
end

def create_events
  events = ['Sassy Chess Collective', 'Go Players for Global Peace', 'Hack the Planet', 'Foo Bar Event', 'Def Dont Miss This', 'Rawwwwrrrrr',
            'Super Cool Thing', 'Fancy Checker Block Party', 'Smash the Snitch Segues', 'Uggh, Creativity Waning', 'Tedious Things',
            'Yeah I know theres probly a Library for This']

  imgs = 1.upto(20).map { |n| "/img/banners/#{n}.jpg" }
  events.each do |e|
    url = e.gsub(' ', '-').gsub('[^a-zA-Z]', '').downcase
    start_time = rando_time
    end_time = start_time + 2.hours
    Event.create(title: e, description: e, event_url: url, banner_url: imgs.sample,
                 location: 'Portland, OR', start_time: start_time, end_time: end_time)
  end
end

def create_relationships
  pdxo4j = Group.first
  last   = Group.last
  admin = User.first

  Group.all.each { |g| admin.groups << g }
  Contact.all.each { |c| c.groups << pdxo4j }
  Event.all.each { |e| e.groups << pdxo4j }

  rando_group = -> { Group.find((1..last.id).to_a.sample) }
  User.all.each { |u| u.groups << rando_group.() unless u.admin }
end

clear_data
create_groups
create_contacts
create_events
create_relationships
