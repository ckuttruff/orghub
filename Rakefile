require 'yaml'
require 'logger'
require 'active_record'

include ActiveRecord::Tasks

class Seeder
  def initialize(seed)
    @seed = seed
  end

  def load_seed
    raise "Seed file '#{@seed}' does not exist" unless File.file? @seed
    load @seed
  end
end


root = File.expand_path '..', __FILE__
DatabaseTasks.env = ENV['RACK_ENV'] || 'development'
conf = File.join root, 'config/database.yml'
DatabaseTasks.database_configuration = YAML.load(File.read(conf))

# Disgusting hack cause yaml's not interpolating environment vars
#  I swear this used to work... not sure when that changed
#  Moving on with my life after some failed searches
%w(default development production test).each do |envir|
  DatabaseTasks.database_configuration[envir]['username'] = ENV['DB_USER']
  DatabaseTasks.database_configuration[envir]['password'] = ENV['DB_PW']
end

DatabaseTasks.db_dir = File.join root, 'db'
DatabaseTasks.migrations_paths = [File.join(root, 'db/migrate')]
DatabaseTasks.seed_loader = Seeder.new(File.join(root, 'db/seeds.rb'))
DatabaseTasks.root = root

task :environment do
  ActiveRecord::Base.configurations = DatabaseTasks.database_configuration
  ActiveRecord::Base.establish_connection DatabaseTasks.env.to_sym
end

load 'active_record/railties/databases.rake'
