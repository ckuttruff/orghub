# orghub

OrgHub is a tool for community organizers.  The plan is to make a toolkit for managing events,
contacts, calendar, mailers, coalition collaboration, etc.  This is a space to directly challenge the
pervasiveness of tools like Facebook events, google calendar, etc.  These companies have proven time and again to be terrible stewards of our data - prioritizing profit over all else.

Many more updates to come, overview of features, video introduction,
etc.  If you are interested in this project, please hit me up in the issues section of github or on the
federation!  ( https://organizing.social/@slackz ).  I would love to collaborate.  This project is for the
community.  You can be certain that the development, process, transparency, and democratic decision making
are firm commitments of OrgHub and PDX Outreach for Justice (a driving organizing force behind the project).

Much love and solidarity!

![Orghub UI preview image](https://github.com/ckuttruff/orghub/blob/master/public/img/preview.png)
