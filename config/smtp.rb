require 'mail'

SMTP_OPTS = {
  address:        ENV['SMTP_SERVER'],
  port:           ENV['SMTP_PORT'],
  domain:         ENV['SMTP_DOMAIN'],
  user_name:      ENV['SMTP_USER'],
  password:       ENV['SMTP_PW'],
  authentication: ENV['SMTP_AUTH_TYPE'],
  enable_starttls_auto: true }

Mail.defaults do
  delivery_method :smtp, SMTP_OPTS
end
