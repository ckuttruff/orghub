require_relative '../../app/controllers/application_controller'
require 'test/unit'

class SmsListTest < Test::Unit::TestCase

  def test_format_number
    numbers = ['503-123-4567', '503.123.4567', '503 123 4567',
               '5031234567', '1503123 4567']

    numbers.each do |num|
      assert( Texting.format_number(num) == '15031234567' )
    end
  end

end
