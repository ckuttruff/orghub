require_relative '../../app/controllers/application_controller'
require 'test/unit'

class SmsListTest < Test::Unit::TestCase
  def setup
    destroy_all

    @l1 = SmsList.create(name: 'pdxo4j')
    @l2 = SmsList.create(name: 'other group')

    @u1 = SmsUser.create(name: 'chrisk',
                         phone: Texting.format_number('503.123.4567'),
                         admin: true,
                         sms_list_id: @l1.id)

    @u2 = SmsUser.create(name: 'user2',
                         phone: Texting.format_number('503.123.4568'),
                         sms_list_id: @l2.id)
  end

  def teardown
    destroy_all
  end

  def test_list_add
    assert(@u2.list_add('chrisk') =~ /Not authorized/)
    assert(@u1.list_add('user2') =~ /Added user to list/)
  end

  def test_list_active
    assert(@u1.list_active =~ /Current active list: pdxo4j/)
    assert(@u2.list_active =~ /Current active list: other group/)
    assert(@u2.list_active('pdxo4j') =~ /Active list updated to: pdxo4j/)
    assert(@u2.list_active('fake list') =~ /List name not found/)
  end

  def test_list_create
    assert(@u2.list_create('foo bar') =~ /Not authorized/)
    assert(@u1.list_create('foo bar') =~ /Created list: foo bar/)
  end

  def test_user_create
    assert(@u2.user_create('newuser', '503.123.4569') =~ /Not authorized/)
    assert(@u1.user_create('newuser', '503.123.4569') =~ /Created user/)
  end

  def test_responder_add
    assert(@u1.responder_add('user2') =~ /Added responder to active list/)
  end

  private

  def destroy_all
    models = [SmsSubscriber, SmsUser, SmsList]
    models.each { |m| m.destroy_all }
  end
end
